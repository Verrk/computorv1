#!/usr/bin/env python

import sys

write = sys.stdout.write

def get_num(s, num):
    i = 0
    minus = 0
    while s[i] == ' ':
        i += 1
    while s[i] != '=' and i < len(s) - 1:
        if s[i] == '-':
            minus = 1
            i += 1
        elif s[i] == '+':
            minus = 0
            i += 1
        while s[i] == ' ':
            i += 1
        j = i
        while (s[j] >= '0' and s[j] <= '9') or s[j] == '.':
            j += 1
        x = float(s[i:j])
        if minus == 1:
            x = -x
        while s[i] != '^':
            i += 1
        i += 1
        j = i
        while (s[j] >= '0' and s[j] <= '9'):
            j += 1
            if j >= len(s):
                break
        p = int(s[i:j])
        i = j
        num['x' + str(p)] = x
        num['d'] = max(num['d'], p)
        if i >= len(s):
            break
        while s[i] == ' ':
            i += 1

def parse(s, num1, num2):
    i = 0
    while s[i] != '=':
        i += 1
    i += 1
    get_num(s[i:], num2)
    for key in num2.keys():
        num1[key] = 0.0
    num1['d'] = num2['d']
    get_num(s, num1)

def get_reduce(num1, num2):
    for key in num2.keys():
        if key != 'd':
            num1[key] = num1[key] - num2[key]
    if num1['d'] == 2 and num1['x2'] == 0:
        num1['d'] = 1
    if num1['d'] == 1 and num1['x1'] == 0:
        num1['d'] = 0
    write("Reduced form: ")
    for i in range(0, num1['d'] + 1):
        write(str(abs(num1['x' + str(i)])).rstrip('0').rstrip('.') + " * X^" + str(i))
        if i + 1 <= num1['d'] and num1['x' + str(i + 1)] < 0:
            write(' - ')
        elif i + 1 <= num1['d'] and num1['x' + str(i + 1)] >= 0:
            write(' + ')
    write(" = 0\n")

def resolve1(num):
    res = -num['x0'] / num['x1']
    print("The solution is \n%d" % res)

def delta_p(num, delta):
    res1 = (-num['x1'] + (delta ** 0.5)) / (2 * num['x2'])
    res2 = (-num['x1'] - (delta ** 0.5)) / (2 * num['x2'])
    print("Discriment is strictly positive, the two solutions are:")
    print(str(res1).rstrip('0').rstrip('.'))
    print(str(res2).rstrip('0').rstrip('.'))

def delta_n(num, delta):
    delta = -delta
    r1 = -num['x1'] / (2 * num['x2'])
    r2 = -num['x1'] / (2 * num['x2'])
    i1 = (-delta ** 0.5) / (2 * num['x2'])
    i2 = -((-delta ** 0.5) / (2 * num['x2']))
    print("Discriment is strictly negative, the two solutions are:")
    if r1 != 0:
        write(str(r1).rstrip('0').rstrip('.'))
    if i1 > 0 and r1 != 0:
        write('+')
    elif i1 < 0:
        write('-')
    if i1 != 0 and abs(i1) != 1:
        write(str(abs(i1)).rstrip('0').rstrip('.'))
    if i1 != 0:
        print("i")
    if r2 != 0:
        write(str(r2).rstrip('0').rstrip('.'))
    if i2 > 0 and r2 != 0:
        write('+')
    elif i2 < 0:
        write('-')
    if i2 != 0 and abs(i2) != 1:
        write(str(i2).rstrip('0').rstrip('.'))
    if i2 != 0:
        print("i")

def resolve2(num):
    delta = num['x1'] - 4 * num['x2'] * num['x0']
    print("Delta = " + str(delta).rstrip('0').rstrip('.'))
    if delta > 0:
        delta_p(num, delta)
    elif delta < 0:
        delta_n(num, delta)
    else:
        res = -num['x1'] / (2 * num['x2'])
        print("Discriminant is zero, the solution is:\n" + str(res).rstrip('0').rstrip('.'))

def resolve0(num):
    if num['x0'] == 0:
        print("The solutions are all the numbers")
    else:
        print("There is no solution")

if __name__ == "__main__":
    argv = sys.argv
    argc = len(argv)
    num1 = {'x0': 0.0, 'x1': 0.0, 'x2': 0.0, 'd': 0}
    num2 = {'x0': 0.0, 'x1': 0.0, 'x2': 0.0, 'd': 0}
    if argc > 1:
        parse(argv[1], num1, num2)
        get_reduce(num1, num2)
        d = num1['d']
        print("Polynomial degree: " + str(num1['d']))
        if d > 2:
            print("The polynomial is strictly greater than 2. I can't solve")
            exit(1)
        elif d == 2:
            resolve2(num1)
        elif d == 1:
            resolve1(num1)
        else:
            resolve0(num1)
